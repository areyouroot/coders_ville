Rules:

- Each round has one hint from us that will help you, but it comes at a cost of -5 points.
- There are four challenges, each with its own points, so be careful. The total marks are 100.
- 10 marks will be for advance challenges from cybersecurity, and 90 marks will be based on your development skills.

Challenge 1 - Warmup (total score: 10)

This is just a warmup game. Go to this link and press challenge 4. There will be two challenges, each carrying 10 marks. You can choose any one of them, and the time for the total task is 10 minutes. This round has no rules because it's not related to your development skills. Your time starts now!

Rules: No rules.

Note: The first challenge needs your Google account to be signed in. You can use any Google account. This challenge is common for all.

Challenge 2 - Debug the code (score: 10)

Go to the link and you can see the directory 3. Inside that, open the file of whichever language you know and debug that code. This challenge is for 20 minutes, and this round is for 10 points.

Rules: Don't use ChatGPT. Use Notepad or any text editor to correct the code and call me or Koushik before running the code. No internet search is allowed.

5-minute break.

Challenge 3 - Algorithm and Console Application (score: 30)

Generate the algorithm or the console application that works.

Rules: Do not use ChatGPT. Use any code editor of your choice. You can use the internet for search, but you are not allowed to search the whole code or any important items in the code. If we catch you, you will get 0 for implementation. Any programming language is allowed.

Marks splitage: 
- 20 marks for implementation and algorithm
- 10 marks for readability, OOPs usage, and others.

Challenge 4 - GUI Application (total marks: 50)

This is the most important challenge, and it involves creating a GUI app. Go to the link and select any.

Rules: Do not use ChatGPT. Use any code editor of your choice. You can use the internet for search, but you are not allowed to search the whole code or any important items in the code. If we catch you, you will get 0 for implementation. Any programming language is allowed.

Marks splitage:
- 10 points for the readability of the code
- 10 points for the GUI
- 30 points for the logic and implementation.